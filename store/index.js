export const state = () => ({
    shop: {},
	product: {},
	user: {},
})

export const mutations = {
    setShop(state, value) {
        state.shop = value
    },
    setProduct(state, value) {
        state.product = value
    },
	setUser(state, value) {
		state.user = value
	}
}
