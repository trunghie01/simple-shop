import Vue from 'vue'

export default (options = {}, onConfirm, onDeny, onCancel) => {
    let swalOptions = options
    swalOptions.reverseButtons = true
    swalOptions.title = options.title ?? 'Alert'
    swalOptions.icon = options.icon ?? 'info'
    if(options.stick){
        swalOptions.allowOutsideClick = false
        swalOptions.allowEscapeKey = false
        swalOptions.showDenyButton = true
    }
    Vue.swal.fire(swalOptions).then(result => {
        if(result.isConfirmed){
            onConfirm?.()
        } else if(result.isDenied){
            onDeny?.()
        }  else if(result.isDismissed){
            onCancel?.()
        } else {
            console.log('swal result', result)
        }
    }).catch(e => console.log(e))
}