export const setCookie = (cname, cvalue, exdays) => {
    const d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    let expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

export const getCookie = (cname) => {
	if(process.server) return ""
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for(let i = 0; i <ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
        }
    }
    return "";
}

export const visit = async (db, appName) => {
    let userVisitToken = getCookie('userVisitToken')
    let dateNow = new Date()
    if(userVisitToken.length == 0){
      let newToken = Math.random().toString(36).substring(2) + Math.random().toString(36).substring(2);
      let lastDate = new Date(dateNow.getFullYear(), dateNow.getMonth() + 1, 0)
  
      setCookie('userVisitToken', newToken, lastDate.getDate() - dateNow.getDate())
      userVisitToken = newToken
    }
  
    let collectionName = 'visitCount.' + appName + dateNow.getMonth()
    let querySnapshot = []
    let thisUserVisitCount = 1
    let userCount = 0
    let visitCount = 0
  
    try{
      querySnapshot = await db.collection(collectionName).get()
    } catch(e){
      console.log(e)
    }
    querySnapshot.forEach((doc) => {
      let data = doc.data()
      if(doc.id == userVisitToken){
        thisUserVisitCount = data.count + 1
      }
      userCount++
      visitCount += data.count
    })
  
    db.collection(collectionName).doc(userVisitToken).set({ count: thisUserVisitCount })
      .catch(e => console.log(e))
    if(thisUserVisitCount == 1){ // if first time visit, add 1 more user, either way, add 1 more visit
      userCount++
    }
    visitCount++
  
    return { userCount, visitCount }
}