import Vue from 'vue'
import { setCookie, getCookie } from '@/utils/cookie.js'
import axios from 'axios'

axios.defaults.baseURL = 'http://127.0.0.1:8000'
axios.defaults.headers.common = {
	'Authorization': 'Bearer ' + getCookie('user-token')
}

Vue.prototype.setCookie = setCookie
Vue.prototype.getCookie = getCookie